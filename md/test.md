<style>

h1 + p:first-letter {
    font-size: 2em;
}

</style>

# Test Header

There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.

##### Test Table Header

|Centered Column |Left-aligned Column                  |
|:---:           |:---                                 |
|A               |Lots of text here                    |
|B               |Lots of text here                    |
|C               |Lots of text here                    |
|D               |Lots of text here                    |

> #### Header 4
> 
> If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.
> All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.
>
> |R | L                  |
> |---:|:---|
> |A    |1                    |
> |B     |2                |

\columnbreak

[anchor-point]:<>

<div>

## Header 2

It uses a dictionary of over 200 ~~Latin~~ Pseudolatin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.

</div>

<img src="https://via.placeholder.com/150" alt="An image"/>

\pagebreakNum

# Here Is Another Page

This one doesn't have a page number tag.

\pagebreak

### And Another One 

This last one does, though.

<div class="pageNumber auto"></div>
